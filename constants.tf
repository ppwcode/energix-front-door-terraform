locals {
  subscription_id     = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
  resource_group_name = "energix"
  domain_name         = "pw-energix.org"
  subdomain_name      = "app"
  ttl                 = 300
  subdomain           = "${local.subdomain_name}.${local.domain_name}"
  frontdoor_name      = "${local.subdomain_name}-${replace(local.domain_name, ".org", "")}"
  frontdoor_host_name = "${local.frontdoor_name}.azurefd.net"
}
