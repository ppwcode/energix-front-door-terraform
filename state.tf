terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstateenergix"
    container_name       = "tfstate-energix"
    key                  = "energix-front-door-terraform.tfstate"
    subscription_id      = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
    use_azuread_auth     = true
  }
}

data "terraform_remote_state" "ui-artifacts" {
  backend = "azurerm"
  config = {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstateenergix"
    container_name       = "tfstate-energix"
    key                  = "energix-ui-artifacts.tfstate"
    subscription_id      = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
    use_azuread_auth     = true
  }
}
