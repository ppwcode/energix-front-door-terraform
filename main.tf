terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

provider "azurerm" {
  features {}

  storage_use_azuread = true
  subscription_id     = local.subscription_id
}

# CNAME record
resource "azurerm_dns_cname_record" "cname" {
  name                = local.subdomain_name
  zone_name           = local.domain_name
  resource_group_name = local.resource_group_name
  ttl                 = local.ttl
  record              = local.frontdoor_host_name
}

# Front-door
resource "azurerm_frontdoor" "fd" {
  name                                         = local.frontdoor_name
  resource_group_name                          = local.resource_group_name
  enforce_backend_pools_certificate_name_check = true

  # Routing rule: HTTP to HTTPS
  routing_rule {
    name               = "http-to-https-routing-rule"
    accepted_protocols = ["Http"]
    patterns_to_match  = ["/*"]
    frontend_endpoints = [
      local.frontdoor_name,
      local.subdomain_name
    ]
    redirect_configuration {
      redirect_protocol = "HttpsOnly"
      redirect_type     = "Found"
    }
  }

  # Routing rule: Forward
  routing_rule {
    name               = "forward-routing-rule"
    accepted_protocols = ["Https"]
    patterns_to_match  = ["/*"]
    frontend_endpoints = [
      local.frontdoor_name,
      local.subdomain_name
    ]
    forwarding_configuration {
      forwarding_protocol = "HttpsOnly"
      backend_pool_name   = "backend-pool"
    }
  }

  # Load balancing
  backend_pool_load_balancing {
    name = "loadbalancingsettings"
  }

  # Health probe
  backend_pool_health_probe {
    name         = "healthprobesettings"
    enabled      = true
    probe_method = "HEAD"
  }

  # Backend pool for "immutable"
  backend_pool {
    name = "backend-pool"
    backend {
      host_header = data.terraform_remote_state.ui-artifacts.outputs.I-ui_artifacts.host
      address     = data.terraform_remote_state.ui-artifacts.outputs.I-ui_artifacts.host
      http_port   = 80
      https_port  = 443
    }
    load_balancing_name = "loadbalancingsettings"
    health_probe_name   = "healthprobesettings"
  }

  # Frontend endpoint (Azure FD)
  frontend_endpoint {
    name                         = local.frontdoor_name
    host_name                    = local.frontdoor_host_name
    session_affinity_enabled     = false
    session_affinity_ttl_seconds = 0
  }

  # Frontend endpoint (custom domain)
  frontend_endpoint {
    name                         = local.subdomain_name
    host_name                    = local.subdomain
    session_affinity_enabled     = false
    session_affinity_ttl_seconds = 0
  }
}

resource "azurerm_frontdoor_custom_https_configuration" "custom_https" {
  frontend_endpoint_id              = azurerm_frontdoor.fd.frontend_endpoints[local.subdomain_name]
  custom_https_provisioning_enabled = true

  custom_https_configuration {
    certificate_source = "FrontDoor"
  }
}
